def calculate_storage(filesize):
    block_size = 4096
    # Use floor division to calculate how many blocks are fully occupied
    full_blocks = block_size//filesize
    # print(full_blocks)  

    #Use the modulo operator to check whether there's any remainder
    partial_block_remainder = filesize % block_size
    print(partial_block_remainder)

    
    # Depending on whether there's a remainder or not, return
    # the total number of bytes required to allocate enough blocks
    # to store your data.
    if partial_block_remainder > 0:
        return full_blocks * filesize
    return block_size * partial_block_remainder

# print(calculate_storage(1))    # Should be 4096
# print(calculate_storage(4096)) # Should be 4096
print(calculate_storage(4097)) # Should be 8192
# print(calculate_storage(6000)) # Should be 8192